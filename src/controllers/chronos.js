const models = require('../models')

module.exports = {
    get: async (req, res, next) => {
        try{
            const chrono = await models.Chronos.findOne({
                where: {
                    id: req.params.id
                },
                attributes: {
                    exclude: ['id']
                }
            })

            res.json(chrono)
        }catch(err){
            next(err)
        }
    },
    getIdEvent: async (req, res, next) => {
        try{
            const chrono = await models.Chronos.findAll({
                where: {
                    id_event: req.params.id
                },
                order: [
                    ['chrono', 'ASC']
                ]
                // attributes: {
                //     exclude: ['id_event']
                // }
            })

            res.json(chrono)
        }catch(err){
            next(err)
        }
    },
    getAll: async (req, res, next) => {
        try{
            const chronos = await models.Chronos.findAll()

            res.json(chronos)
        }catch(err){
            next(err)
        }
    },
    create: async (req, res, next) => {
        try{
            const {name, email, phone, chrono, date, id_event} = req.body
    
            const {id} = await models.Chronos.create({
                name,
                email,
                phone,
                chrono,
                date,
                id_event
            })

            res.status(201).json(id)
        }catch(err){
            next(err)
        }
    },
    update: async (req, res, next) => {
        try{
            const {name, email, phone, chrono, date, id_event} = req.body

            const rows = await models.Chronos.update({
                name,
                email,
                phone,
                chrono,
                date,
                id_event
            }, {
                where: {
                    id: req.params.id
                }
            })

            res.json(rows[0] > 0)
        }catch(err){
            next(err)
        }
    },
    delete: async (req, res, next) => {
        try{
            const rows = await models.Chronos.destroy({
                where: {
                    id: req.params.id
                }
            })

            res.json(rows > 0)
        }catch(err){
            next(err)
        }
    },
    // getEvent: async (req, res, next) => {
    //     try{
    //         const tables = await models.Tables.findAll({
    //             attributes: {
    //                 exclude: ['id_event']
    //             },
    //             where: {
    //                 room: req.params.id
    //             }
    //         })

    //         res.json(tables)
    //     }catch(err){
    //         next(err)
    //     }
    // },
    // addEvent: async (req, res, next) => {
    //     try{
    //         const {number, name} = req.body

    //         const {id} = await models.Tables.create({
    //             name,
    //             number, name,
    //             room: req.params.id
    //         })

    //         res.json(id)
    //     }catch(err){
    //         next(err)
    //     }
    // },
}