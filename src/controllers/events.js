const models = require('../models')

module.exports = {
    get: async (req, res, next) => {
        try{
            const event = await models.Events.findOne({
                where: {
                    id: req.params.id
                },
                attributes: {
                    exclude: ['id']
                }
            })

            res.json(event)
        }catch(err){
            next(err)
        }
    },
    getAll: async (req, res, next) => {
        try{
            const events = await models.Events.findAll()

            res.json(events)
        }catch(err){
            next(err)
        }
    },
    create: async (req, res, next) => {
        try{
            const {name, date, lieu, track, game} = req.body
    
            const {id} = await models.Events.create({
                name,
                date,
                lieu,
                track,
                game
            })

            res.status(201).json(id)
        }catch(err){
            next(err)
        }
    },
    update: async (req, res, next) => {
        try{
            const {name, date, lieu, track, game} = req.body
    
            const rows = await models.Events.update({
                name,
                date,
                lieu,
                track,
                game
            }, {where: {
                    id: req.params.id
                }
            })

            res.json(rows[0] > 0)
        }catch(err){
            next(err)
        }
    },
    delete: async (req, res, next) => {
        try{
            const rows = await models.Events.destroy({
                where: {
                    id: req.params.id
                }
            })

            res.json(rows > 0)
        }catch(err){
            next(err)
        }
    }
}