const {Client} = require('pg')
const client = new Client({
    user: 'postgres',
    password: 'root',
    database: 'speedrace'
})

client.connect()

module.exports = client