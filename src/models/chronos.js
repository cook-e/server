/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Chronos = sequelize.define('Chronos', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'id',
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'email'
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'phone'
    },
    chrono: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'chrono'
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'date'
    },
    id_event: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'id_event'
    }
  }, {
    tableName: 'chronos'
  })

  return Chronos
};
