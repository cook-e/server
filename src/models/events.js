/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Events = sequelize.define('Events', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'date'
    },
    lieu: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'lieu'
    },
    track: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'track'
    },
    game: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'game'
    },
  }, {
    tableName: 'events'
  })

  Events.associate = models => {
    Events.belongsTo(models.Chronos, {
      foreignKey: 'id',
      targetKey: 'id_event',
      as: 'Chronos'
    })
  }

  return Events
};
