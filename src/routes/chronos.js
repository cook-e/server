const router = require('express').Router()

const ChronosController = require('../controllers/chronos')

router.route('/:id')
    .get(ChronosController.get)
    .patch(ChronosController.update)
    .delete(ChronosController.delete)

router.route('/event/:id')
    .get(ChronosController.getIdEvent)

router.route('/')
    .get(ChronosController.getAll)
    .post(ChronosController.create)

module.exports = router