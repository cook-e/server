const router = require('express').Router()

const EventsController = require('../controllers/events')

router.route('/:id')
    .get(EventsController.get)
    .patch(EventsController.update)
    .delete(EventsController.delete)

router.route('/')
    .get(EventsController.getAll)
    .post(EventsController.create)

module.exports = router