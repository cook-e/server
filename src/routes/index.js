const events = require('./events')
const chronos = require('./chronos')

module.exports = app => {
    app.use('/events', events)
    .use('/chronos', chronos)
}