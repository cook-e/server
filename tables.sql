drop table if exists events cascade;
create table events(
    id serial primary key,
	name varchar(100) not null,
	date date not null,
	lieu varchar(100),
	track varchar(100),
	game varchar(100)
);
insert into events (id, name, date, lieu, track, game) values
(1, 'Rassemblement Renault Sport & Alpine', '2019-06-08', 'Aix-les-Bains', 'Vineda - Corto', 'Dirt 4'),
(2, '24h du Mans', '2019-07-20', 'Le Mans', 'Circuit Le Mans', 'Asseto Corsa');



drop table if exists chronos cascade;
create table chronos(
    id serial primary key,
	name varchar(100) not null,
	email varchar(100) not null,
	phone varchar(100) not null,
	chrono varchar(100) not null,
	date date not null,
	id_event int
        references events (id)
);
insert into chronos (id, name, email, phone, chrono, date, id_event) values
(1, 'Benjamin', 'benjamin.leroy73100@gmail.com', '0601043447', '1.02.250', '2019-06-08'),
(2, 'Lambert', 'lambert.latour@gmail.com', '0606060606', '1.01.200', '2019-06-09'),
(3, 'Vincent', 'vincent.latour@gmail.com', '0606060606', '2.09.814', '2019-07-20'),
(4, 'Anthony', 'anthony.leroy@gmail.com', '0606060606', '2.04.814', '2019-07-20');